<?php
require("common.php");

$emailLog = $_POST['emailLog'];
$password = $_POST['password'];

$submitted_username = '';

if (!empty($_POST)) {
    $query = " 
            SELECT 
                id, 
                username, 
                password, 
                salt, 
                email 
            FROM users 
            WHERE 
                email = :emailLog 
        ";

    $query_params = array(
        ':emailLog' => $_POST['emailLog']
    );

    try {
        // Execute the query against the database
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);
    } catch (PDOException $ex) {
        die("Failed to run query: " . $ex->getMessage());
    }

    $login_ok = false;

    $row = $stmt->fetch();
    if ($row) {
        $check_password = hash('sha256', $_POST['password'] . $row['salt']);
        for ($round = 0; $round < 65536; $round++) {
            $check_password = hash('sha256', $check_password . $row['salt']);
        }

        if ($check_password === $row['password']) {
            // If they do, then we flip this to true
            $login_ok = true;
        }
    }

    if ($login_ok) {
        unset($row['salt']);
        unset($row['password']);

        $_SESSION['user'] = $row;

//        header("Location: private.php");
//        die("Redirecting to: private.php");
        die ("1");
    } else {
        // Tell the user they failed
        print("Falha no Login.");

        $submitted_username = htmlentities($_POST['emailLog'], ENT_QUOTES, 'UTF-8');
    }
}
?>