<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Grupo BCZ</title>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

    <link rel="stylesheet" href="css/global.css">
    <!-- <script src="js/jquery.min.js"></script> -->
    <!-- <script src="js/jquery-2.1.4.min.js"></script> -->
    <!--    <script src="js/paymentCard.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>

    <script src="js/slides.min.jquery.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
<!--    <script src="js/validator.js"></script>-->
    <script src="js/login.js"></script>
    <script>
        $(function () {
            $('#slides').slides({
                preload: true,
                preloadImage: 'img/loading.gif',
                play: 5000,
                pause: 2500,
                hoverPause: true
            });
        });
    </script>
    <script src="js/tabcontent.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/style_tabs.css"/>
    <script type="text/javascript" src="js/modernizr.custom.04022.js"></script>
    <link rel="stylesheet" href="css/styles-menu.css">
    <script src="js/modernizr.custom.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="js/login.js"></script>
    <!--<link rel="stylesheet" type="text/css" href="css/component_botao.css" />-->
    <!-- <script src="js/contact.js"></script> -->

</head>
<body>
<div id="wrapper">
