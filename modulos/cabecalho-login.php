<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

<!------ Include the above in your HEAD tag ---------->


<div class="navbar navbar-default navbar-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--            <a class="navbar-brand" href="#">Login dropdown</a>-->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="conteudo.php?p=protecao-patrimonial">Proteçao Patrimonial</a></li>
                <!--                            <li class="active"><a href="conteudo.php?p=protecao-patrimonial"><span>Proteçao Patrimonial</span></a></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Seguros <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="dropdown-submenu">
                            <a href="#">Responsabilidade Civil</a>
                            <ul class="dropdown-menu">
                                <li><a href="conteudo.php?p=responsabilidade-civil-advogado">Advogados</a></li>
                                <li><a href="conteudo.php?p=responsabilidade-civil-contabilistas">Contabilistas</a></li>
                                <li><a href="conteudo.php?p=responsabilidade-civil-profissionais-saude">Profissionais de Saúde</a></li>
                                <li><a href="conteudo.php?p=responsabilidade-civil-outros">Outros</a></li>
                            </ul>
                        </li>
                        <li><a href="conteudo.php?p=garantia">Garantia</a></li>
                        <li><a href="conteudo.php?p=aeronaves-embarcacoes">Aeronaves / Embarcaçoes</a></li>
                        <li><a href="https://<?php echo $_SERVER['HTTP_HOST'];?>/grupobcz/conteudo.php?p=saude-odontologico">Saúde / Odontológico</a></li>
                        <li><a href="conteudo.php?p=patrimonial">Patrimonial</a></li>
                        <li><a href="conteudo.php?p=gestao-de-beneficios">Gestão de Benefícios</a></li>
                        <li><a href="conteudo.php?p=vida">Vida</a></li>
                        <li><a href="conteudo.php?p=previdencia-privada">Previdência Privada</a></li>
                        <li><a href="conteudo.php?p=credito">Crédito</a></li>
                        <li><a href="conteudo.php?p=educacional">Educacional</a></li>
                        <li><a href="conteudo.php?p=massificado">Massificado</a></li>
                        <li><a href="conteudo.php?p=ambiental">Ambiental</a></li>
                    </ul>
                </li>
                <li><a href="conteudo.php?p=administracao-servicos">Administração de serviços</a></li>
                <li><a href="conteudo.php?p=sobre-nos">Sobre nós</a></li>
                <li><a href="conteudo.php?p=fale-conosco">Fale Conosco</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <!--                <li><p class="navbar-text">Already have an account?</p></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                    <ul id="login-dp" class="dropdown-menu">
                        <li>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Login</h4>
                                    <!-- <div class="alert alert-danger" role="alert" id="error" style="display: none;">...</div> -->
                                    <div id="returnmessageLogin"></div>
                                    <form class="navbar-form navbar-right" role="form" id="login-form" name="login-form" method="post" accept-charset="UTF-8">
                                        <div class="form-group">
                                            <input name="emailLog" id="emailLog" type="email" tabindex="1" class="form-control" placeholder="E-mail" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Senha" required>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default" name="login-submit" id="login-submit">Login</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="bottom text-center">
                                    Novo aqui? <a href="conteudo.php?p=cadastrarUsuario"><b>Cadastre-se</b></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</div>
<script src="js/login.js"></script>