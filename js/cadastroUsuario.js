$(document).ready(function() {
    $("#submitUsuario").click(function() {
        var email = $("#emailCad").val();
        var password = $("#passwordCad").val();
        var nomeCad = $("#nomeCad").val();
        $("#returnmessage").empty(); // To empty previous error/success message.
        var er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/;
        if (email == '' || password == '' || nomeCad == '') {
            jQuery('#returnmessage').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>Por favor, informe os campos.</p></div>");
            return;
        } if (!er.exec(email) ) {
            jQuery('#returnmessage').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>Por favor, informe um e-mail válido.</p></div>");
            return;
        } else {
            // Returns successful data submission message when the entered information is stored in database.
            $.post("login/register.php", {
                email: email,
                nomeCad: nomeCad,
                password: password
            }, function(data) {
                if (data == 1) {
                    jQuery('#returnmessage').html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>×</button><p>Seus dados foram salvos com sucesso.</p></div>");
                    $("#cad-usuario")[0].reset(); // To reset form fields on success.
                    return;
                }else{
                    jQuery('#returnmessage').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>"+ data +"</p></div>");
                    return;
                }
            });
        }
    });
});