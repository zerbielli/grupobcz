$(document).ready(function() {
    $("#submit").click(function() {
        var name = $("#name").val();
        var email = $("#email").val();
        var message = $("#message").val();
        var contact = $("#contact").val();
        $("#returnmessage").empty(); // To empty previous error/success message.
        var er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/;
        if (name == '' || email == '' || message == '') {
            //        alert("Por favor, informe os campos");
            jQuery('#returnmessage').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>Por favor, informe os campos!.</p></div>");
            return false;
        } if (!er.exec(email) ) {
            jQuery('#returnmessage').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>Por favor, informe um e-mail válido!.</p></div>");
            return false;
        } else {
            // Returns successful data submission message when the entered information is stored in database.
            $.post("/contact_form.php", {
                name1: name,
                email1: email,
                message1: message,
                contact1: contact
            }, function(data) {
                if (data == 1) {
                    jQuery('#returnmessage').html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>×</button><p>Obrigado por entrar em contato conosco! Em breve lhe retornaremos.</p></div>");
                    $("#form")[0].reset(); // To reset form fields on success.
                    return false;
                }else{
                    $("#returnmessage").append(data); // Append returned message to message paragraph.
                    return false;
                }
            });
        }
    });
});