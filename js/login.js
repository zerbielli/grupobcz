$(document).ready(function () {
    $("#login-submit").click(function () {
        var emailLog = $("#emailLog").val();
        var password = $("#password").val();
        $("#returnmessageLogin").empty(); // To empty previous error/success message.
        var er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/;
        if (emailLog == '') {
            jQuery('#returnmessageLogin').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>Por favor, informe seu e-mail.</p></div>");
            return;
        } else if (password == '') {
            jQuery('#returnmessageLogin').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>Por favor, informe sua senha.</p></div>");
            return;
        } else if (!er.exec(emailLog)) {
            jQuery('#returnmessageLogin').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>Por favor, informe um e-mail válido.</p></div>");
            return;
        } else {
            // Returns successful data submission message when the entered information is stored in database.
            $.post("login/login.php", {
                emailLog: emailLog,
                password: password
            }, function (data) {
                if (data == "1") {
                    location.reload();
                    return true;
                } else if (data !== "") {
                    jQuery('#returnmessageLogin').html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><p>" + data + "</p></div>");
                    return false;
                }
            });
        }
    });
});