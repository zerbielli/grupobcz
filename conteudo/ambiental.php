<div id="content">

    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro Ambiental</span>
            <br><br>
            É um instrumento que visa garantir a reparação de danos (pessoais ou materiais) causados involuntariamente a terceiros, em decorrência de poluição ambiental,
            com o objetivo de ressarcimento das despesas e indenizações, resultantes de responsabilidade civil atribuída pelo judiciário.<br>
            O Seguro Ambiental ainda não é muito difundido no Brasil. Entretanto esta área apresenta grande potencial de crescimento, principalmente devido as
            exigências legais e as pressões cada vez maiores da comunidade, dos órgãos fiscalizadores e da média.
            <br><br>
            Considerando as exigências legais, umas das penas restritivas instituídas no Art. 12, da Lei nº 9.605/98 (Lei de Crimes Ambientais) é a prestação pecuniária
            que "consiste no pagamento em dinheiro é vítima ou é entidade pública ou privada com fim social, de importância, fixada pelo juiz, não inferior a um salário
            mínimo nem superior a trezentos e sessenta salários mínimos. O valor pago será deduzido do montante de eventual reparação civil a que for condenado o infrator".
            <br><br>
            Mesmo possuindo um sistema de controle ambiental bem estabelecido, a empresa está sujeita a problemas que não estavam previstos, pois a resposta do meio ambiente
            nem sempre é aquela esperada. Neste contexto o seguro ambiental torna-se importante para a cobertura (total ou parcial) de prejuízos decorrentes de eventuais
            problemas ambientais causados a terceiros.
            <br><br><br><br><br><br><br>
        </span>
    </div>
</div>
