<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>

<style type="text/css" media="screen">
    .has-error input {
        border-width: 2px;
    }

    .validation.text-danger:after {
        content: 'Validation failed';
    }

    .validation.text-success:after {
        content: 'Validation passed';
    }
</style>

<!--<script>-->
<!--    jQuery(function($) {-->
<!--        $('[data-numeric]').payment('restrictNumeric');-->
<!--        $('.cc-number').payment('formatCardNumber');-->
<!--        $('.cc-exp').payment('formatCardExpiry');-->
<!--        $('.cc-cvc').payment('formatCardCVC');-->
<!---->
<!--        $.fn.toggleInputError = function(erred) {-->
<!--            this.parent('.form-group').toggleClass('has-error', erred);-->
<!--            return this;-->
<!--        };-->
<!---->
<!--        $('form').submit(function(e) {-->
<!--            e.preventDefault();-->
<!---->
<!--            var cardType = $.payment.cardType($('.cc-number').val());-->
<!--            $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));-->
<!--            $('.cc-exp').toggleInputError(!$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));-->
<!--            $('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));-->
<!--            $('.cc-brand').text(cardType);-->
<!---->
<!--            $('.validation').removeClass('text-danger text-success');-->
<!--            $('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');-->
<!--        });-->
<!---->
<!--    });-->
<!--</script>-->


<div id="bx-conteudo">
    <span class="text-center">
        <h3>Seguro de Responsabilidade Civil para Dentistas</h3>
        <h5>O Seguro de Responsabilidade Civil protege você dentista contra possíveis processos de pacientes. Ele cobre tanto gastos com advogados quanto indenizações. Este seguro é coberto pela Chubb, que é a maior seguradora do mundo.</h5>
    </span>
    <section class="section highlight-gray" id="custo">
        <div class="section-header text-center">
            <h1 class="section-title">Quanto custa?</h1>
            <h2 class="section-subtitle">Selecione a melhor opção</h2>
        </div>
            <div class="row text-center">
                <div class="col-md-4 col-sm-4">
                    <div class="panel panel-info" style="border-color:#9e9c9c">
                        <div class="panel-heading">
                            Cobertura de até
                            <br>
                            <strong>R$ 50.000,00</strong>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">
<!--                                <div class="row">-->
<!--                                    Primeiro mês grátis-->
<!--                                </div>-->
                                <div class="row hidden-sm hidden-xs">
                                    <div class="col-md-7 text-right" id="valor">R$29</div>
                                    <div class="col-md-5 text-left" id="valor-menor">
                                        <div id="centavos">,90</div>
                                        <div id="mensais">mensais</div>
                                    </div>
                                </div>
                                <div class="row visible-sm visible-xs">
                                    <div class="col-md-7" id="valor">R$29,90</div>
                                    <div class="col-md-5" id="valor-menor">
                                        <div id="mensais">mensais</div>
                                    </div>
                                </div>
                                <label class="radio-inline"><input type="radio" name="optradio" checked>Opção 1</label>
<!--                                <a class="btn btn-success">Contratar</a>-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="panel panel-danger" style="border-color:#9e9c9c">
                        <div class="panel-heading">
                            Cobertura de até
                            <br>
                            <strong>R$ 100.000,00</strong>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">
<!--                                <div class="row">-->
<!--                                    Primeiro mês grátis-->
<!--                                </div>-->
                                <div class="row hidden-sm hidden-xs">
                                    <div class="col-md-7 text-right" id="valor">R$34</div>
                                    <div class="col-md-5 text-left"id="valor-menor">
                                        <div id="centavos">,90</div>
                                        <div id="mensais">mensais</div>
                                    </div>
                                </div>
                                <div class="row visible-sm visible-xs">
                                    <div class="col-md-7" id="valor">R$34,90</div>
                                    <div class="col-md-5" id="valor-menor">
                                        <div id="mensais">mensais</div>
                                    </div>
                                </div>
                                <label class="radio-inline"><input type="radio" name="optradio">Opção 2</label>
<!--                                <a class="btn btn-success">Contratar</a>-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="panel panel-success" style="border-color:#9e9c9c">
                        <div class="panel-heading">
                            Cobertura de até
                            <br>
                            <strong>R$ 200.000,00</strong>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">
<!--                                <div class="row">-->
<!--                                    Primeiro mês grátis-->
<!--                                </div>-->
                                <div class="row hidden-sm hidden-xs">
                                    <div class="col-md-7 text-right" id="valor">R$49</div>
                                    <div class="col-md-5 text-left" id="valor-menor">
                                        <div id="centavos">,90</div>
                                        <div id="mensais">mensais</div>
                                    </div>
                                </div>
                                <div class="row visible-sm visible-xs">
                                    <div class="col-md-7 valor">R$49,90</div>
                                    <div class="col-md-5 valor-menor">
                                        <div class="mensais">mensais</div>
                                    </div>
                                </div>
                                <label class="radio-inline"><input type="radio" name="optradio">Opção 3</label>
<!--                                <a class="btn btn-success">Contratar</a>-->
                            </li>
                        </ul>
                    </div>
                </div>
        </div>
    </section>
    <section class="section highlight-gray">
        <div class="section-header text-center">
            <h1 class="section-title">Formas de pagamento</h1>
            <h2 class="section-subtitle">Escolha sua forma de pagamento</h2>
        </div>
        <div class="row text-center">
            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Boleto</div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            Pague seu seguro anual via boleto
                            <br/>
                            <br/>
<!--                            <br/>-->
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#boletoModal">
                                Gerar Boleto
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Cartão</div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            Pague seu seguro anual via cartão de crédito em até 12 vezes
                            <br/>
                            <br/>
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#cartaoModal">
                                Informar dados do cartão
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
            <!--<div class="col-md-4 col-sm-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Assinatura</div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            Pague com cartão mensalmente
                            <br/>
                            <br/>
                            <br/>
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#assinaturaModal">
                                Informar dados do cartão
                            </button>
                        </li>
                    </ul>
                </div>
            </div>-->
            <span>Em caso de arrependimento você pode cancelar sua assinatura, veja <a id="dLabel"
                                                                                       data-target="#boletoCancelamento"
                                                                                       data-toggle="modal">aqui</a> nossa política de cancelamento.</span>
        </div>
    </section>
    <div><span class="text-center"><h6>CNPJ: 30.423.486/0001-31</h6></span></div>
</div>

<div class="modal fade" id="boletoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Boleto</h3>
                <!--                    <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">-->
            </div>
            <div class="modal-body">
                <form role="form" data-toggle="validator" id="payment-form" method="POST">
                    <div id="returnmessage"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><b>Dados Pessoais</b></div>
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                <label>Nome</label>
                                <input class="form-control" type="text" required name="boletoNome" id="boletoNome">
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="form-label">CPF</label>
                                <input class="form-control" id="cpf" type="text" required name="boletoCpf">
                            </div>
                            <div class="form-group col-sm-9">
                                <label class="form-label">e-Mail</label>
                                <input class="form-control" type="email" id="boletoEmail"
                                       data-error="Informe um e-mail válido." required name="boletoEmail">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <p style="font-size: 11px;">* Ao contratar esse seguro você concorda com os
                        <a data-toggle="modal" data-target="#termosModal">
                            <strong>Termos de Uso </strong>
                        </a>
                    </p>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="submit" onclick="pagarBoleto()">Confirmar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--    Cartão -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="cartaoModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Cartão de Credito</h3>
                <!--                    <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">-->
            </div>
            <div class="modal-body">
                <form role="form" id="payment-form" method="POST" novalidate autocomplete="on">
                    <div id="returnmessage"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dados do cartão</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                <label class="form-label">Nome do Títular do Cartão</label>
                                <input type="text" class="form-control" name="card-holder-name" id="card-holder-name"
                                       placeholder="Nome impresso no cartão">
                            </div>
                            <div class="form-group col-md-9">
                                <label class="form-label" for="cc-number">Número do cartão</label>
                                <input type="tel" class="form-control cc-number" name="cc-number" id="cc-number"
                                       autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required data-numeric>

                                <!--                                    <label for="cc-number" class="control-label">Card number formatting <small class="text-muted">[<span class="cc-brand"></span>]</small></label>-->
                                <!--                                    <input id="cc-number" type="tel" class="input-lg form-control cc-number" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required>-->
                            </div>
                            <div class="form-group col-md-3">
                                <label class="form-label" for="cardCVC">Código de Segurança</label>
                                <input type="text" class="form-control" name="cardCVC" id="cvv" data-numeric
                                       maxlength="3">
                                <!--                                        <small class="form-text text-muted">Número impresso no verso do cartão, composto de três dígitos</small>-->
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label" for="expiry-month">Validade</label>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control col-sm-2" name="validade-mes" id="validade-mes">
                                            <option>Mês</option>
                                            <option value="01">Janeiro (01)</option>
                                            <option value="02">Fevereiro (02)</option>
                                            <option value="03">Março (03)</option>
                                            <option value="04">Abril (04)</option>
                                            <option value="05">Maio (05)</option>
                                            <option value="06">Junho (06)</option>
                                            <option value="07">Julho (07)</option>
                                            <option value="08">Agosto (08)</option>
                                            <option value="09">Setembro (09)</option>
                                            <option value="10">Outubro (10)</option>
                                            <option value="11">Novembro (11)</option>
                                            <option value="12">Dezembro (12)</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <select class="form-control" name="validade-ano">
                                            <option value="18">2018</option>
                                            <option value="19">2019</option>
                                            <option value="20">2020</option>
                                            <option value="21">2021</option>
                                            <option value="22">2022</option>
                                            <option value="23">2023</option>
                                            <option value="24">2024</option>
                                            <option value="25">2025</option>
                                            <option value="26">2026</option>
                                            <option value="27">2027</option>
                                            <option value="28">2028</option>
                                            <option value="29">2029</option>
                                            <option value="30">2030</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="validation"></h2>
                </form>
                <p style="font-size: 11px;">* Ao contratar esse seguro você concorda com os
                    <a data-toggle="modal" data-target="#termosModal">
                        <strong>Termos de Uso </strong>
                    </a>
                </p>
            </div>
            <div class="row" style="display:none;">
                <div class="col-xs-12">
                    <p class="payment-errors"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="subscribe btn btn-primary" id="subscribe">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<!--    Assinatura-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="assinaturaModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Assinatura</h3>
                <!--                    <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">-->
            </div>
            <div class="modal-body">
                <form role="form" data-toggle="validator" id="payment-form" method="POST" action="javascript:void(0);">
                    <div id="returnmessage"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><b>Dados Pessoais</b></div>
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                <label>Nome</label>
                                <input class="form-control" type="text" required name="customer[name]">
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="form-label">CPF</label>
                                <input class="form-control" type="text" id="cpf" required
                                       name="customer[document_number]">
                            </div>
                            <div class="form-group col-sm-9">
                                <label class="form-label">e-Mail</label>
                                <input class="form-control" type="email" id="inputEmail"
                                       data-error="Informe um e-mail válido." required name="customer[email]">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>Endereço</b></div>
                        <div class="panel-body">
                            <div class="form-group col-md-3">
                                <label class="form-label">CEP</label>
                                <input class="form-control" type="text" required="true"
                                       name="customer[address][zipcode]">
                            </div>
                            <div class="form-group col-md-9">
                                <label class="form-label">Logradouro</label>
                                <input class="form-control" type="text" required="true"
                                       name="customer[address][street]">
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="form-label">Número</label>
                                <input class="form-control" type="number" required="true"
                                       name="customer[address][street_number]">
                            </div>
                            <div class="form-group col-sm-5">
                                <label class="form-label">Complemento</label>
                                <input class="form-control" type="text" name="customer[address][complementary]">
                            </div>
                            <div class="form-group col-sm-4">
                                <label class="form-label">Bairro</label>
                                <input class="form-control" type="text" required="true"
                                       name="customer[address][neighborhood]">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Cidade</label>
                                <input class="form-control" type="text" required="true" name="customer[address][city]">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Estado</label>
                                <input class="form-control" type="text" required="true" name="customer[address][state]">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><b>Telefone</b></div>
                        <div class="panel-body">
                            <div class="form-group col-md-2">
                                <label class="form-label">DDD</label>
                                <input class="form-control" type="number" required="true" name="customer[phone][ddd]"
                                       maxlength="2">
                            </div>
                            <div class="form-group col-md-10">
                                <label class="form-label">Número</label>
                                <input class="form-control" type="number" required="true" name="customer[phone][number]"
                                       maxlength="9">
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dados do cartão</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                <label class="form-label">Nome do Títular do Cartão</label>
                                <input type="text" class="form-control" name="card-holder-name" id="card-holder-name"
                                       placeholder="Nome impresso no cartão">
                            </div>
                            <div class="form-group col-md-9">
                                <label class="form-label" for="card-number">Número do cartão</label>
                                <input type="text" class="form-control" name="card-number" id="card-number"
                                       placeholder="Número impressso no cartão">
                            </div>
                            <div class="form-group col-md-3">
                                <label class="form-label" for="cvv">Código de Segurança</label>
                                <input type="text" class="form-control" name="cvv" id="cvv">
                                <!--                                        <small class="form-text text-muted">Número impresso no verso do cartão, composto de três dígitos</small>-->
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label" for="expiry-month">Validade</label>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control col-sm-2" name="validade-mes" id="validade-mes">
                                            <option>Mês</option>
                                            <option value="01">Janeiro (01)</option>
                                            <option value="02">Fevereiro (02)</option>
                                            <option value="03">Março (03)</option>
                                            <option value="04">Abril (04)</option>
                                            <option value="05">Maio (05)</option>
                                            <option value="06">Junho (06)</option>
                                            <option value="07">Julho (07)</option>
                                            <option value="08">Agosto (08)</option>
                                            <option value="09">Setembro (09)</option>
                                            <option value="10">Outubro (10)</option>
                                            <option value="11">Novembro (11)</option>
                                            <option value="12">Dezembro (12)</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <select class="form-control" name="validade-ano">
                                            <option value="18">2018</option>
                                            <option value="19">2019</option>
                                            <option value="20">2020</option>
                                            <option value="21">2021</option>
                                            <option value="22">2022</option>
                                            <option value="23">2023</option>
                                            <option value="24">2024</option>
                                            <option value="25">2025</option>
                                            <option value="26">2026</option>
                                            <option value="27">2027</option>
                                            <option value="28">2028</option>
                                            <option value="29">2029</option>
                                            <option value="30">2030</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <p style="font-size: 11px;">* Ao contratar esse seguro você concorda com os
                    <a data-toggle="modal" data-target="#termosModal">
                        <strong>Termos de Uso </strong>
                    </a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" id="submit">Confirmar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="termosModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Termos de Uso</h3>
                <!--                    <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">-->
            </div>
            <div class="modal-body">
                <div>
                    <p>
                        O usuário irá receber um certificado em até 48 (quarenta e oito) horas após o pedido de compra
                        do seguro.
                        O referido certificado indicará que o usuário
                        está devidamente assegurado na importância contratada, no seguro feito pelo Grupo BCZ,
                        na apólice em emissão, tendo todas as informações complementares nas condições
                        gerais disponível no site do Grupo BCZ e no site da Superintendência de Seguros
                        Privados - SUSEP. O seguro será reajustado anualmente seguindo o indicador IPCA-
                        Índice Nacional de Preços ao Consumidor Amplo
                    </p>

                    <p>
                        Declara o usuário, neste ato, na qualidade de pessoa física, ser absolutamente capaz diante das
                        leis civis brasileiras. Ser
                        maior de dezoito anos, ou menor emancipado, não interditado, gozando de plenas faculdades
                        mentais e plena capacidade de contratar, sem vontade de causar dano, prejuízo, prestar
                        falsa informação, fraude ou estelionato.
                    </p>

                    <p>
                        Toda informação ou dado pessoal prestado pelo usuário é armazenado nos arquivos eletrônicos do
                        site do Grupo BCZ, o
                        qual tomará todas as medidas possíveis para manter a confidencialidade e a segurança
                        das mesmas, enviando-as somente aos destinos necessários para a devida prestação
                        dos serviços, bem como não responderá por prejuízo eventualmente originado na violação
                        dessas medidas por terceiros que por ventura se apropriem da informação.
                    </p>

                    <p>
                        Em nenhuma circunstância, o Grupo BCZ, seus diretores ou colaboradores serão responsáveis por
                        quaisquer danos diretos
                        ou indiretos, especiais, incidentais ou de consequência, perdas ou despesas oriundas
                        da conexão com este website ou uso da sua parte ou incapacidade de uso por qualquer
                        parte, ou com relação a qualquer falha de desempenho, erro, omissão, interrupção,
                        defeito ou demora na operação ou transmissão, vírus de computador ou falha da linha
                        ou do sistema.
                    </p>

                    <p>
                        O seguro será renovado automaticamente, após os primeiros 12 meses da contratação, podendo ser
                        cancelado sem qualquer multa
                        após o referido prazo.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="boletoCancelamento" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Cancelamento</h3>
            </div>
            <div class="modal-body">
                <p>
                    O segurado pode realizar o pedido de cancelamento através de uma carta de próprio punho
                    informando seus dados, datado e assinado para o e-mail <a href="mailto:atendimento@grupobcz.com.br">atendimento@grupobcz.com.br</a> e este
                    cancelamento será processado em até 30 dias (trinta) dias úteis.
                <p>
                    Para os casos em que o segurado tiver crédito a receber em relação a prêmios já quitados,
                    como nos casos de pagamento integral do prêmio à vista, a devolução do valor proporcional
                    será realizada em até 90 (noventa) dias corridos, contados da data do pedido de cancelamento
                    do contrato.
                </p>
                    O cancelamento da renovação automática do seguro mensal não afetará o período de
                    cobertura já quitado.
                <p>
                    Para mais informações entre em contato nos telefones <a href="tel:1130852737">(11) 3085-2737</a> / <a href="tel:1130852734">(11) 3085-2734</a> ou no
                    e-mail <a href="mailto:atendimento@grupobcz.com.br">atendimento@grupobcz.com.br</a>.
                </p>
                </p>
            </div>
        </div>
    </div>
</div>

<script src="js/pagamento.js"></script>
<!--     <script src="js/jquery.payment.js"></script>-->
<script src="https://assets.pagar.me/pagarme-js/3.1/pagarme.min.js"></script>
<!--    <script src="js/paymentCard.js"></script>-->
