<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguros Massificados</span>
            <br><br>
            Os seguros massificados geralmente são oferecidos no momento da compra de um produto ou do pagamento de uma
            mensalidade de determinado serviço.
            <br><br>
            Implantados pelo Grupo BCZ em empresas que buscam ampliar o valor de seu portfólio de clientes, criando novas
            fontes de receita legal e diferenciais que aumentam a satisfação e a fidelização nos canais B2B e B2C.
            <br><br>
            <span id="bold">Projetos mais comuns:</span>
            <br><br>
            - Proteção contra desemprego<br>
            - Assistência residencial ou veicular<br>
            - Proteção financeira em empréstimos<br>
            - Garantia estendida em eletrodomésticos
            <br><br>
            Consulte nos também sobre outros projetos.
            <br><br><br><br><br><br><br><br><br>
        </span>
    </div>
</div>
