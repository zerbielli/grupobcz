<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Previdência Privada</span>
            <br><br>
            O Grupo BCZ auxilia seus clientes que desejam investir em previdência privada no Brasil e no exterior,
            neste último, garantindo uma aposentadoria com solidez em moeda estrangeira.
            <br><br>
            No Brasil, existem dois tipos de previdência privada:
            <br><br>
            <div class="tabs">
                <div class="tab">
                    <input class="tab-radio" type="radio" id="tab-1" name="tab-group-1" checked>
                    <label class="tab-label" for="tab-1">PGBL</label>
                    <div class="tab-panel">
                        <div class="tab-content" id="h5">
                            <span id="h5">
                                O PGBL é um modelo utilizado para quem possui renda, faz declaração completa do IR e
                                contribui para a previdência social, e pretende poupar parte dela para sua aposentadoria.
                                Vale ressaltar que no caso do PGBL os valores poupados, ou seja, investidos no PGBL,
                                podem ser abatidos do imposto de renda até o limite de 12% da renda anual tributável.
                                Investir em VGBL também é uma forma de adiar o pagamento do imposto de renda para o momento
                                do resgate, ou seja não há cobrança de IR semestral (come cotas), fazendo no longo prazo com
                                que os valores acumulados com juros ao longo do tempo sejam maiores. Na hora do resgate,
                            o IR incide sobre todo o valor resgatado (valor investido + remuneração).
                            </span>
                        </div>
                    </div>
                </div>
                <div class="tab1"></div>
                <div class="tab">
                    <input class="tab-radio" type="radio" id="tab-2" name="tab-group-1">
                    <label class="tab-label" for="tab-2">VGBL</label>
                    <div class="tab-panel">
                        <div class="tab-content" id="h5">
                            <span id="h5">
                                O VGBL é um modelo utilizado para pessoas físicas que dispôem de recursos no presente e
                                desejam aplica-los com vista a garantir uma aposentadoria. É também utilizado para fins de
                                sucessão, de maneira que os recursos investidos em VGBL não entram no espólio nem no
                                inventário desde que respeitando as determinações legais. É indicado também para quem não
                                contribui para a previdência social, para quem deseja aplicar além dos 12% de sua renda
                                tributável já investida em PGBL, para quem é isento ou faz declaração simplificada de IR.
                                No VGBL, na hora do resgate o imposto incide somente sobre a rentabilidade acumulada.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            Ambos, VGBL e PGBL, deve-se se escolher a tabela que se desejará utilizar. O critério é a perspectiva de
            quando irá necessitar dos recursos, de maneira que a tabela privilegia a poupança a longo prazo.
            Para casos de resgates antecipados na tabela progressiva, a tributação terá a alíquota única de 15%.
            Na tabela regressiva, no momento do resgate ou no recebimento de renda, a incidência de IR ocorre de forma
            definitiva e exclusiva na fonte, começando em 35%, com redução de 5% a cada 2 anos, até atingir 10% para prazos
            acima de 10 anos.
            <br><br>
        </span>
    </div>
</div>
