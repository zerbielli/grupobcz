<?php
include("modulos/menu1.php");
?>
<div id="content">
    <div id="bx-conteudo">
        <span id="h4">
            <span id="h1-2">Seguro de Responsabilidade Civil</span>
            <br><br>
            O Grupo BCZ tem longa experiência e know how em assessorar clientes pessoa física e jurídica na melhor
            contratação de um seguro de responsabilidade civil, orientando sobre as melhores condições contratuais,
            riscos e coberturas, e também de custo/benefício para o cliente.
            <br><br>
            O sistema jurídico brasileiro, acompanhando o cenário já existente nos Estados Unidos e Europa, tem se
            tornado o país muito mais litigioso, tanto pelos aprimoramentos na legislação quanto no maior acesso ao
            conhecimento e a justiça por parte da população. Observa-se decisões cada vez mais favoráveis ao reclamante
            e uma jurisprudência muito mais severa.
            <br><br>
            Seguro de responsabilidade civil profissional para os profissionais dos ramos:
            <br><br>
            <div class="tabs">

                <div class="tab">
                    <input class="tab-radio" type="radio" id="tab-1" name="tab-group-1" checked>
                    <label class="tab-label" for="tab-1">Advocacia</label>

                    <div class="tab-panel">
                        <div class="tab-content" id="h5">
                            <span id="h5">
                                O Seguro de Responsabilidade civil para advogado visa cobrir as reclamações de terceiros,
                                principalmente clientes, decorrentes dos danos causados pelas falhas dos serviços
                                profissionais prestados pelo escritório.
                                <br><br>
                                <span id="bold">Coberturas mais comuns:</span>
                                <br><br>
                                - Perda de prazos<br>
                                - Perda de chance<br>
                                - Desídia<br>
                                - Quebra de sigilo profissional<br>
                                - Perda/extravio de documentos<br>
                                - Responsabilidade por decisões estratégicas em <span id="italic">due dilligence</span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="tab1"></div>

                <div class="tab">
                    <input class="tab-radio" type="radio" id="tab-2" name="tab-group-1">
                    <label class="tab-label" for="tab-2">Contabilista</label>

                    <div class="tab-panel">
                        <div class="tab-content" id="h5">
                            <span id="h5">
                                O Seguro de Responsabilidade Civil Profissional para Contabilistas é uma garantia para o
                                profissional preservar seu patrimônio, uma vez que os escritórios de contabilidade não
                                estão isentos de cometer falhas, prejudicando assim seus clientes, ainda mais considerando
                                o alto grau de especialização exigido do profissional e a complexa legislação tributária
                                brasileira.
                                <br><br>
                                <span id="bold">Coberturas mais comuns:</span>
                                <br><br>
                                - condenação por danos materiais e morais causados pelo segurado em decorrência de negligência, imprudência ou imperêcia cometida durante a prestação de serviço;<br>
                                - as perdas financeiras, incluindo os lucros cessantes, bem como perda, roubo e extravio de documentos do cliente em responsabilidade do segurado;<br>
                                - os custos de defesa com advogados e demais despesas relacionadas ao processo e a defesa do segurado.
                                <br><br>
                                <span id="bold">Benefícios:</span>
                                <br><br>
                                O Seguro de Responsabilidade Civil para Contadores também é um diferencial para o contabilista perante seus concorrentes, pois é uma garantia para qualquer eventualidade que possa estar sujeito a erros e danos.
                            </span>
                        </div>
                    </div>
                </div>

                <div class="tab1"></div>

                <div class="tab">
                    <input class="tab-radio" type="radio" id="tab-3" name="tab-group-1">
                    <label class="tab-label" for="tab-3">Profisionais da Sa�de</label>

                    <div class="tab-panel">
                        <div class="tab-content" id="h5">
                            <span id="h5">
                                Médicos, dentistas, fisioterapeutas, psicólogos, hospitais, laboratórios, clínicas,
                                nutricionistas, fonoaudiólogos e profissões similares.
                                <br><br>
                                <span id="bold">Coberturas mais comuns</span>
                                <br><br>
                                - Erro ou omissão no exercício da profissão: Indenizações pelos danos ocorridos com o
                                paciente sejam eles Danos Corporais, Danos Morais e/ou Danos Materiais<br>
                                - RC Geral - Acidentes de uso ou conservação do consultório<br>
                                - Defesa Jurídica - garantia de defesa jurídica em qualquer esfera judicial. Seja ela
                                cívil, criminal ou administrativa Desde que relacionado ao risco Segurado<br>
                                - Serviço de Assistência - consultas, orientação e/ou acompanhamento perante autoridades,
                                quando necessário<br>
                                - Honorários advocatícios - pagamento de custas judiciais e/ou de peritos, com ou sem
                                condenação do Segurado decorrentes de reclamações dos pacientes
                                <br><br>
                                <span id="bold">Benefícios:</span>
                                <br><br>
                                Flexibilidade para a customização de coberturas para riscos específicos;<br>
                                Sigilo absoluto na gestão das informações de nossos clientes;
                                <br><br>
                                *Não existem restrições para tempo de experiência profissional.
                            </span>
                        </div>
                    </div>
                </div>

            </div>
            <br>
            Consulte nos também para Seguros de Responsabilidade Civil Profissional para:
            <br><br>
            - Consultorias<br>
            - Engenharia e arquitetura<br>
            - Tecnologia da informação<br>
            - Cartórios<br>
            - Agências de turismo<br>
            - Escritórios de Engenharia<br>
            - Outros
            <br><br>
        </span>
    </div>
</div>
