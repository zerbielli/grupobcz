<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Proteção Patrimonial</span>
            <br><br>
            <span id="bold">Proteção Pessoal</span><br><br>
            Tranquilidade financeira é um privilégio que não pertence necessariamente é uma parte exclusiva da sociedade,
            esta tranquilidade se dá através de planejamento e orientação financeira. Esse é o negócio do Grupo BCZ e ele
            está ao seu alcance neste momento.
            <br><br>
            Nós colocamos em suas mãos propostas pensadas sob medida para atender suas expectativas e necessidades específicas
            a fim de resguardar seu patrimônio e futuro de forma inteligente, planejada e rentável, para que assim você possa
            sonhar e planejar seus objetivos de maneira muito mais confiante.
            <br><br>
            <center><img src="images/pes.jpg" width="" alt="" title=""></center>
            <br><br><br>
            <span id="bold">Proteção Empresarial</span><br><br>
            Sucessão empresarial pode até ser um assunto pouco discutido entre os diretores e sócios das corporações,
            e talvez justamente por isso, muitas não estejam preparadas para enfrentar uma situação de vital importância
            para a sobrevivência do seu negócio, mas, com certeza você vai se surpreender com as diversas soluções que o
            Grupo BCZ pode oferecer nestes casos.
            <br><br>
            Dificilmente são provisionados nas empresas recursos para a aquisição das cotas de participação do sócio ausente.
            Por consequência disso, muitas vezes, herdeiros legais podem assumir posições de liderança para as quais não
            estão preparados e muito frequentemente isto leva a uma desaceleração dos negócios, perda de valor da empresa e
            até mesmo ao encerramento das atividades da mesma.
            <br><br>
            O Grupo BCZ tem soluções financeiras sólidas para ajuda-los na sucessão societária os auxiliando financeiramente
            aos advogados e contadores da sua empresa.
            <br><br>
            <center><img src="images/emp.jpg" width="" alt="" title=""></center>
            <br><br><br>
            <span id="bold">Proteção Familiar</span><br><br>
            O planejamento da segurança familiar para o futuro é a preocupação de todo chefe de família que toma sobre si a responsabilidade de
            prover para seus entes queridos. Sabendo disso o Grupo BCZ auxilia você neste planejamento de Proteção financeira familiar,
            garantindo que a sua família esteja sempre tranquila e protegida.
            <br><br>
            De forma personalizada, desenhamos um planejamento adequado para proteção da 2º idade e provisão para a 3º idade, assunto muito
            preocupante na sociedade brasileira atual.
            <br><br>
            Consideramos também um planejamento financeiro para a educação dos filhos, projetos/sonhos financeiros (1º automóvel, festa de 15 anos,
            intercambio educacional, casamento).
            <br><br>
            A Proteção financeira familiar é a forma mais inteligente e responsável de prevenir você e sua família de contratempos relacionados a
            sua saúde e, por isso, o Grupo BCZ se torna um parceiro indispensável.
            <br><br>
            <center><img src="images/fam.jpg" width="" alt="" title=""></center>
        </span>
    </div>
</div>
