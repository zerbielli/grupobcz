<div id="content">

    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro de Aeronaves e Embarcações</span>
            <br><br>
            <div class="tabs">

                <div class="tab">
                    <input class="tab-radio" type="radio" id="tab-1" name="tab-group-1" checked>
                    <label class="tab-label" for="tab-1">Aeronaves</label>

                    <div class="tab-panel">
                        <div class="tab-content" id="h5">
                            <span id="h5">
                                <span id="bold">Seguro de responsabilidade civil (RETA)</span>
                                <br><br>
                                Toda aeronave, independentemente de sua operação ou utilização, deve possuir cobertura de seguro de responsabilidade civil (RETA),
                                correspondente a sua categoria de registro.
                                A contratação do seguro RETA é obrigatória para todo o explorador (proprietário ou arrendatário) conforme previsto em Lei.
                                <br><br>
                                <span id="bold">Seguro Aeronáutico</span>
                                <br><br>
                                O Seguro aeronáutico visa proteger o equipamento em si:
                                <br><br>
                                <span id="bold">Coberturas mais comuns</span>
                                <br><br>
                                - Danos causados por acidentes;<br>
                                - Roubo total;<br>
                                - Avaria particular;<br>
                                - Incêndio.
                            </span>
                        </div>
                    </div>
                </div>

                <div class="tab1"></div>

                <div class="tab">
                    <input class="tab-radio" type="radio" id="tab-2" name="tab-group-1">
                    <label class="tab-label" for="tab-2">Embarcações</label>

                    <div class="tab-panel">
                        <div class="tab-content" id="h5">
                            <span id="h5">
                                <span id="bold">Seguro de Danos Pessoais Causados por Embarcações ou por sua Carga</span>
                                <br><br>
                                (DPEM) O DPEM ou Seguro de Danos Pessoais Causados por Embarcações ou por sua Carga, é um seguro obrigatório, normatizado pela Lei 8374,
                                de 30 de dezembro de 1991. É obrigatório para todos os proprietários de embarcações sejam elas marítimas, fluviais ou lacustres (em lagos).<br>
                                Sua exclusiva finalidade é dar cobertura a pessoas, transportadas ou não, inclusive aos proprietários, tripulantes e ou condutores das embarcações,
                                e a seus respectivos beneficiários ou dependentes.
                                <br><br>
                                <span id="bold">Seguro Náutico</span>
                                <br><br>
                                Já o seguro Náutico visa proteger a embarcação em si.
                                <br><br>
                                <span id="bold">Coberturas mais comuns</span>
                                <br><br>
                                - Perda Total;<br>
                                - Assistência e Salvamento;<br>
                                - Responsabilidade Civil por Abalroação (choque entre duas embarcações);<br>
                                - Avaria Particular (perda parcial);<br>
                                - Roubo e/ou Furto Total Qualificado da Embarcação;<br>
                                - Retirada e Colocação na água;<br>
                                - Translado e sua Guarda.
                            </span>
                        </div>
                    </div>
                </div>

            </div>
            <br>
            Consulte-nos também sobre outras coberturas.
            <br><br>
        </span>
    </div>

</div>
