<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>

<div id="bx-conteudo">
    <section class="section highlight-gray" id="custo">
        <div class="section-header text-center">
            <h1 class="section-title">Cadastro de usuário</h1>
<!--            <h2 class="section-subtitle">Selecione a melhor opção</h2>-->
        </div>

        <div class="row">
            <div class="rover-primary-col rover-single-col col-center">
                <div class="rover-primary-content-block">
                    <div class="new-design js-alerts-container" id="returnmessage"></div>
                    <div id="returnmessage"></div>
                    <form role="form" data-toggle="validator" id="cad-usuario" method="POST">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Dados Pessoais</b></div>
                            <div class="panel-body">
                                <div class="form-group">       
                                    <label for="textNome" class="control-label">Nome</label>        
                                    <input id="nomeCad" name="nomeCad" class="form-control" placeholder="Digite seu nome completo" type="text" required>      
                                </div>
                                <div class="form-group">
                                    <label class="form-label">E-mail</label>
                                    <input class="form-control" type="email" id="emailCad" required name="emailCad" required>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label for="passwordCad" class="control-label">Senha</label>      
                                        <input type="password" class="form-control" id="passwordCad" name="passwordCad" placeholder="Digite sua Senha..." data-minlength="6" required>     
                                        <span class="help-block">Mínimo de seis (6) digitos</span>   
                                    </div>         
                                    <div class="form-group col-lg-6">      
                                        <label for="inputConfirm" class="control-label">Confirme a Senha</label>        
                                        <input type="password" class="form-control" id="inputConfirm" placeholder="Confirme sua Senha..." data-match="#passwordCad" data-match-error="Atenção! As senhas não estão iguais." required>     
                                        <div class="help-block with-errors"></div>    
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="button" class="btn btn-primary" id="submitUsuario">Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="js/cadastroUsuario.js"></script>
<script src="js/validator.js"></script>