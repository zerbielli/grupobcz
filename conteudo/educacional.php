<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro Educacional</span>
            <br><br>
            O Seguro educacional garante ao aluno em caso de desemprego, invalidez ou morte do responsável,
            o pagamento das despesas com a educação. O Grupo BCZ faz implantação de seguro educacional em colégios.
            faculdades e cursos, trazendo diversos benefícios para os alunos e para a instituição de ensino.
            O seguro educacional também possui coberturas para:
            <br><br>
            - Acidentes pessoais do aluno dentro da instituição de ensino<br>
            - Responsabilidade civil da instituição de ensino para com o aluno<br>
            - Proteção para a instituição contra quebra de contrato de ensino pelo aluno por doença ou acidente.
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        </span>
    </div>
</div>
