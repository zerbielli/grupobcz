<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Sobre nós</span>
            <br><br>
            <span id="bold">Grupo BCZ</span><br>
            Nossa equipe formada por especialistas em <span id="italic">Risk Solutions</span> é referência no
            mercado na área de administração de seguros, gestão global de custos securitários das corporações e
            gerenciamento de contratos de seguros.<br>
            Somos especializados na contratação de seguros de ramos específicos e de alta complexidade, assim como na
            gestão de carteiras de seguros e benefícios.
            <br><br>
            <span id="bold">Consulte-nos sobre seguros:</span><br>
            Aeronaves e embarcações<br>
            <span id="italic">Affinity</span><br>
            Ambiental<br>
            Educacional<br>
            Engenharia<br>
            Fusões e Aquisições <br>
            Massificados<br>
            Responsabilidade Civil<br>
            Risco Operacional<br>
            Saúde e odontológico<br>
            Seguro de Crédito<br>
            Vida
            <br><br>
            <span id="bold">Nossa Missõo</span><br>
            Administrar carteiras de seguros gerando valor para os clientes
            <br><br>
            <span id="bold">Nossa visão</span><br>
            Ser o principal parceiro de serviços de nossos clientes levando as melhores soluções contratuais e de custo do mercado.
            <br><br>
            <span id="bold">Principais indústrias:</span><br>
            Alimentação<br>
            Automobilástica<br>
            Construção<br>
            Comércio e Investimento internacional<br>
            Energia e utilidades<br>
            Ensino<br>
            Varejo<br>
            Logística<br>
            Bancos e fundos de investimento<br>
            Petróleo e Gás
            <br><br>
            Investimos em talentos. Envie seu currículo para nós.
            <br><br>
        </span>
    </div>
</div>
