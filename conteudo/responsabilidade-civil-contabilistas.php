<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro de Responsabilidade Civil - Contabilistas</span>
            <br><br>
            O Grupo BCZ tem longa experiência e <span id="italic">know how</span> em assessorar clientes pessoa física e
            jurídica na melhor contratação de um seguro de responsabilidade civil, orientando sobre as melhores condições
            contratuais, riscos e coberturas, e também de custo/benefício para o cliente.
            <br><br>
            O sistema jurídico brasileiro, acompanhando o cenário já existente nos Estados Unidos e Europa, tem se tornado o
            país muito mais litigioso, tanto pelos aprimoramentos na legislação quanto no maior acesso ao conhecimento e a
            justiça por parte da população. Observa-se decisões cada vez mais favoráveis ao reclamante e uma jurisprudência
            muito mais severa.
            <br><br>
            O Seguro de Responsabilidade Civil Profissional para Contabilistas é uma garantia para o profissional preservar
            seu patrimônio, uma vez que os escritórios de contabilidade não estão isentos de cometer falhas, prejudicando
            assim seus clientes, ainda mais considerando o alto grau de especialização exigido do profissional e a complexa
            legislação tributária brasileira.
            <br><br>
            <span id="bold">Coberturas mais comuns:</span>
            <br><br>
            - condenação por danos materiais e morais causados pelo segurado em decorrência de negligência, imprudência ou
            imperência cometida durante a prestação de serviço;<br>
            - as perdas financeiras, incluindo os lucros cessantes, bem como perda, roubo e extravio de documentos do
            cliente em responsabilidade do segurado;<br>
            - os custos de defesa com advogados e demais despesas relacionadas ao processo e a defesa do segurado.
            <br><br>
            <span id="bold">Benefícios:</span>
            <br><br>
            O Seguro de Responsabilidade Civil para Contadores também é um diferencial para o contabilista perante seus
            concorrentes, pois é uma garantia para qualquer eventualidade que possa estar sujeito a erros e danos.
            <br><br>
            Ligue já para um de nossos consultores e iremos assessorá-lo na contratação da solução em seguros mais
            adequada as suas necessidades: <span id="bold">11 3085-2737</span>
            <br><br>
            Ou, se preferir, simule e contrate online:
            <br><br>
            <center>
                <span id="bold">DESCONTO PARA ASSOCIADOS AOS CONSELHOS, SINDICATOS E ASSOCIACOES DE CLASSE</span>
            </center>
            <br><br>
            Seguro de Responsabilidade Civil Profissional para Contabilistas<br>
            <a href="http://goo.gl/8WFGBR" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
            <br><br>
        </span>
    </div>
</div>
