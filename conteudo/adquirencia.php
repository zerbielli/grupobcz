  <div id="content">

    <div id="bx-conteudo" align="justify">
    <span id="h4">
    <span id="h1-2">Adquir�ncia</span>
    <br><br>
    <div style="float: right;">
    <img src="images/adq1.png" width="200px" style="margin-left: 20px; margin-bottom: 20px;"><br>
    <img src="images/adq2.png" width="200px" style="margin-left: 20px; margin-bottom: 20px;">
	</div>
    O Grupo BCZ � formado por especialistas em acquiring, sendo refer�ncia na �rea de administra��o de adquir�ncia e gerenciamento de contratos da �rea.<br><br>

    Cada adquirente possui um foco de atua��o de acordo com sistema, centros de processamento, custo operacional por transa��o MDR, custo da demanda contratada de rede de telefonia/celular, modelo de equipamentos utilizados no varejo, rede de assist�ncia POS/TEF/Ecommerce, custo financeiro de capta��o, volume, entre outros fatores.
    Estes fatores impactam na qualidade do servi�o prestado e no custo operacional, repassado ao cliente na forma de taxa e aluguel de equipamento.<br><br>

    Somos especialistas em adquir�ncia e nosso papel � escolher dentre as empresas do mercado, qual apresenta a melhor solu��o custo-benef�cio ao cliente, uma vez que cada adquirente tem um foco de atua��o, e desta maneira apresenta custos maiores ou menores de acordo com 4 fatores que analisamos (custo unit�rio de transa��o- MDR, ticket m�dio, fee da bandeira e interc�mbio banc�rio).
     <br><br>
    Oferecemos uma proposta nova de trabalho aqui no Brasil, mas muito comum no mercado americano, a do consultor de adquir�ncia. Para isto, dispomos de um sistema no qual conseguimos fazer simula��es de c�lculos nas diferentes empresas e chegar ao melhor prestador para o seu neg�cio, fazendo poucas perguntas.
    <br><br>
    Nossos clientes podem permanentemente contar com  todo assessoramento do Grupo BCZ, no contato com as empresas, auxiliando na r�pida resolu��o de qualquer problema ou solicita��o, eliminado a necessidade de ser atendido por sistema via  0800, e contando com especialista em adquir�ncia que realmente poder�o buscar as melhores solu��es dentro dos fornecedores da �rea.<br><br>
    Conhe�a esta nova proposta do consultor de adquir�ncia, que j� � um sucesso h� mais de 10 anos no mercado americano, e chega agora ao Brasil neste movimento de consolida��o e de melhoria nos servi�os, aliado a redu��o de taxas. Profissionalize sua �rea de adquir�ncia, reduza seus custos, melhore o servi�o prestado.
	<br><br>
	<center><img src="images/icons.jpg" width="800px"></center>
    </span>
    </div>

  </div>
