<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro Garantia</span>
            <br><br>
            O objetivo do seguro garantia é garantir o cumprimento de um contrato e suas obrigações, assim como de construção,
            fabricação, fornecimento e prestação de serviços.
            <br><br>
            O seguro garantia é uma excelente opção para as empresas oferecerem garantias a seus parceiros sem que com isso
            necessitem destinar seus recursos de capital de giro para ficarem bloqueados neste sentido, ou ainda terem de
            comprometer seus limites de crédito juntos as instituições financeiras. O Grupo BCZ assessora seus clientes na
            escolha e contratação de seguro garantia em diversas modalidades:
            <br><br>
            - Garantias para Adiantamento de Pagamentos;<br>
            - Garantia aduaneira, a favor da Receita Federal;<br>
            - Garantia para Crédito de ICMS.<br>
            - Garantia para execução de contratos;<br>
            - Garantia Judicial;<br>
            - Garantia de proposta;<br>
            - Garantia para substituição de retenções de pagamento;<br>
            - Garantia de Perfeito Funcionamento;
            <br><br>
            Consulte-nos também sobre outras coberturas.
            <br><br><br><br>
        </span>
    </div>
</div>
