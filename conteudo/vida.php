<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro de Vida</span>
            <br><br>
            O Seguro de Vida visa garantir uma indenização aos beneficiários ou herdeiros de um segurado no
            caso de seu falecimento. Vários seguimentos empresariais possuem em suas convenções coletivas
            obrigação de contratar seguro de vida para os funcionários.
            <br><br>
            O Grupo BCZ tem produtos estruturados para atender as convenções coletivas de sindicatos em todo o
            Brasil, além de produtos específicos para pessoas físicas e jurídicas.
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        </span>
    </div>
  </div>
