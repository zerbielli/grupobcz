<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>

<div id="bx-conteudo">
    <section class="section highlight-gray" id="custo">
        <div class="section-header text-center">
            <h1 class="section-title">Minha Conta</h1>
<!--            <h2 class="section-subtitle">Selecione a melhor opção</h2>-->
        </div>

        <div class="row">
            <div class="rover-primary-col rover-single-col col-center">
                <div class="rover-primary-content-block">
                    <div class="new-design js-alerts-container" id="returnmessage"></div>
                    <div id="returnmessage"></div>
                    <form role="form" data-toggle="validator" id="edit-account" method="POST">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Dados Pessoais</b></div>
                            <div class="panel-body">
                                <div class="form-group col-md-12">
                                    <label>Nome</label>
                                    <input class="form-control" type="text" required name="userName" id="userName"
                                           value="<?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?>">
                                </div>
                                <div class="form-group col-sm-9">
                                    <label class="form-label">e-Mail</label>
                                    <input class="form-control" type="email" id="userEmail"
                                           data-error="Informe um e-mail válido." required name="userEmail"
                                           value="<?php echo htmlentities($_SESSION['user']['email'], ENT_QUOTES, 'UTF-8'); ?>">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="button" class="btn btn-primary" id="submitAccount">Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="js/editAccount.js"></script>
<script src="js/validator.js"></script>