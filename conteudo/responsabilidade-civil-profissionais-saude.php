<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro de Responsabilidade Civil - Profissionais de Saúde</span>
            <br><br>
            <img src="images/profissionaissaude.jpg" width="760" height="" title="">
            <br><br>
            <span id="bold">Grupo BCZ é Especialista em seguros para profissionais de saúde</span>
            <br><br>
            <span id="bold">Assessoria jurídica e seguro de responsabilidade civil profissional</span>
            <br><br>
            Quem pode ser segurado:<br>
            Médicos, dentistas, fisioterapeutas, psicólogos, hospitais, laboratórios, clínicas, nutricionistas,
            fonoaudiólogos e profissões similares.
            <br><br>
            <span id="bold">Proteja seu patrimõnio e reputação</span>
            <br><br>
            Os profissionais e clínicas de saúde estão diariamente sujeitos a situações em que podem gerar algum tipo de
            questionamento. Para isto, devem contar com um seguro de responsabilidade civil com coberturas adequadas,
            que possibilite ao profissional todo um suporte para auxiliá-lo na resolução, seja através de acordo, processo
            judicial ou ainda no pagamento de uma indenização.
            <br><br>
            O sistema jurídico brasileiro, acompanhando o cenário já existente nos Estados Unidos e Europa, tem sofrido
            constantes aprimoramentos na legislação, tornando o país muito mais litigioso, observando decisões cada vez mais
            favoráveis ao reclamante e criando uma jurisprudência muito mais severa.
            <br><br>
            Poder contar com auxílio de profissionais com alta experiência no assunto na resolução de eventuais
            questionamentos e ainda no caso de uma indenização ter seu patrimônio protegido, além de tudo, não custa muito,
            observe:
            <br><br>
            Estimativa de custo mensal para algumas especialidades médicas:
            <br><br>
            <center>
                <table width="550" cellspacing="1" cellpadding="4" bgcolor="#44546A">
                    <tr>
                        <td colspan="2" bgcolor="#44546A"><span id="h9">TABELA DE CUSTO MENSAL PARA VALORES SEGURADOS DE R$ 100.000,00</span></td>
                    </tr>
                    <tr>
                        <td bgcolor="#D6DCE4"><span id="h4">ESPECIALIDADE</span></td>
                        <td bgcolor="#D6DCE4"><span id="h4">VALOR MENSAL</span></td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff"><span id="h4">CARDIOLOGIA</span></td>
                        <td bgcolor="#ffffff" align="right"><span id="h4">33,34</span></td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff"><span id="h4">CIRURGIA GERAL</span></td>
                        <td bgcolor="#ffffff" align="right"><span id="h4">97,27</span></td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff"><span id="h4">CIRURGIA PLÁSTICA</span></td>
                        <td bgcolor="#ffffff" align="right"><span id="h4">164,83</span></td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff"><span id="h4">GINECOLOGIA E OBSTETRIA</span></td>
                        <td bgcolor="#ffffff" align="right"><span id="h4">164,83</span></td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff"><span id="h4">ODONTOLOGIA</span></td>
                        <td bgcolor="#ffffff" align="right"><span id="h4">53,66</span></td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff"><span id="h4">OFTALMOLOGIA</span></td>
                        <td bgcolor="#ffffff" align="right"><span id="h4">33,34</span></td>
                    </tr>
                </table>
            </center>
            <br>
            * Para mais informações sobre outras especialidades médicas ou valores, por favor consulte-nos.
            <br><br>
            <span id="bold">Principais coberturas</span>
            <br><br>
            - Erro ou omissão no exercício da profissão: Indenizações pelos danos ocorridos com o paciente sejam eles Danos Corporais, Danos Morais e/ou Danos Materiais<br>
            - RC Geral - Acidentes de uso ou conservação do consultório<br>
            - Defesa Jurídica - garantia de defesa jurídica em qualquer esfera judicial, seja ela cível, criminal ou
            administrativa, desde que relacionado ao risco segurado<br>
            - Serviço de Assistência - consultas, orientação e/ou acompanhamento perante autoridades, quando necessário<br>
            - Honorários advocatícios - pagamento de custas judiciais e/ou de peritos, com ou sem condenação do Segurado
            decorrentes de reclamações dos pacientes<br>
            - Cobertura extensiva para Pessoa Jurídica
            <br><br>
            <span id="bold">Benefícios:</span>
            <br><br>
            - Flexibilidade para a customização de coberturas para riscos específicos;<br>
            - Sigilo absoluto na gestão das informações de nossos clientes;<br>
            - Não existem restrições para tempo de experiência profissional;<br>
            - Possibilidade de acordo na maioria das contratações;<br>
            - Assessoria jurídica especializada 24 horas.
            <br><br>
            <span id="bold">Principais seguradoras:</span>
            <br><br>
            <table width="100%" cellspacing="2" cellpadding="2">
                <tr>
                    <td align="center"><img src="images/logo01.jpg"></td>
                    <td align="center"><img src="images/logo02.jpg"></td>
                    <td align="center"><img src="images/logo03.jpg"></td>
                </tr>
                <tr>
                    <td align="center"><img src="images/logo04.jpg"></td>
                    <td align="center"><img src="images/logo05.jpg"></td>
                    <td align="center"><img src="images/logo06.jpg"></td>
                </tr>
            </table>
            <br><Br>
            Ligue já para um de nossos consultores e iremos assessorá-lo na contratação da solução em seguros mais
            adequada as suas necessidades: <span id="bold">11 3085-2737</span>
            <br><br>
            Ou, se preferir, simule e contrate online:
            <br><br>
            <center>
                <span id="bold">DESCONTO PARA ASSOCIADOS AOS CONSELHOS, SINDICATOS E ASSOCIACOES DE CLASSE</span>
            </center>
            <br><br>
            <table width="760" cellspacing="3" cellpadding="3">
                <tr>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para<br> Médicos<br>
                        <a href="http://goo.gl/9fDhRN" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para<br> Dentistas<br>
                        <a href="http://goo.gl/tmSIaj" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                </tr>
            </table>
            <br><br>
        </span>
    </div>
</div>
