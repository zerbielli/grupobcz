<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro de Responsabilidade Civil - Advogados</span>
            <br><br>
            O Grupo BCZ tem longa experiência e <span id="italic">know how</span> em assessorar clientes pessoa física e
            jurídica na melhor contratação de um seguro de responsabilidade civil, orientando sobre as melhores condições
            contratuais, riscos e coberturas, e também de custo/benefício para o cliente.
            <br><br>
            O sistema jurídico brasileiro, acompanhando o cenário já existente nos Estados Unidos e Europa, tem se
            tornado o país muito mais litigioso, tanto pelos aprimoramentos na legislação quanto no maior acesso ao
            conhecimento e a justiça por parte da população.
            Observa-se decisões cada vez mais favoráveis ao reclamante e uma jurisprudência muito mais severa.
            <br><br>
            O Seguro de Responsabilidade civil para advogado visa cobrir as reclamações de terceiros, principalmente clientes,
            decorrentes dos danos causados pelas falhas dos serviços profissionais prestados pelo escritório.
            <br><br>
            <span id="bold">Coberturas mais comuns:</span>
            <br><br>
            - Perda de prazos<br>
            - Perda de chance<br>
            - Desídia<br>
            - Quebra de sigilo profissional<br>
            - Perda/extravio de documentos<br>
            - Responsabilidade por decisões estratégicas em <span id="italic">due dilligence</span>
            <br><br>
            Ligue ja para um de nossos consultores e iremos assessorá-lo na contratação da solução em seguros mais adequada
            as suas necessidades: <span id="bold">11 3085-2737</span>
            <br><br>
            Ou, se preferir, simule e contrate online:
            <br><br>
            <center>
                <span id="bold">DESCONTO PARA ASSOCIADOS AOS CONSELHOS, SINDICATOS E ASSOCIACOES DE CLASSE</span>
            </center>
            <br><br>
            Seguro de Responsabilidade Civil Profissional para Advogados<br>
            <a href="http://goo.gl/MfMfMZ" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
            <br><br>
        </span>
    </div>
</div>
