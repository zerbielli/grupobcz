<div id="content">
    <div id="bx-conteudo" align="justify">
        <span id="h4">
            <span id="h1-2">Seguro de Responsabilidade Civil - Outros</span>
            <br><br>
            O Grupo BCZ tem longa experiência e <span id="italic">know how</span> em assessorar clientes pessoa física e
            jurídica na melhor contratação de um seguro de responsabilidade civil, orientando sobre as melhores condições
            contratuais, riscos e coberturas, e também de custo/benefício para o cliente.
            <br><br>
            O sistema jurídico brasileiro, acompanhando o cenário já existente nos Estados Unidos e Europa, tem se tornado
            o país muito mais litigioso, tanto pelos aprimoramentos na legislação quanto no maior acesso ao conhecimento e a
            justiça por parte da população. Observa-se decisões cada vez mais favoráveis ao reclamante e uma jurisprudência
            muito mais severa.
            <br><br>
            <!--Consulte nos tamb�m para Seguros de Responsabilidade Civil Profissional para:
    <br><br>
    - Consultorias<br>
    - Engenharia e arquitetura<br>
    - Tecnologia da informa��o<br>
    - Cart�rios<br>
    - Ag�ncias de turismo<br>
    - Escrit�rios de Engenharia<br>
    - Outros
    <br><br><br><br-->
            Ligue já para um de nossos consultores e iremos assessorá-lo na contratação da solução em seguros mais adequada as
            suas necessidades: <span id="bold">11 3085-2737</span>
            <br><br>
            Ou, se preferir, simule e contrate online:
            <br><br>
            <center>
                <span id="bold">DESCONTO PARA ASSOCIADOS AOS CONSELHOS, SINDICATOS E ASSOCIACOES DE CLASSE</span>
            </center>
            <br><br>
            <table width="760" cellspacing="3" cellpadding="3">
                <tr>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para Corretores de imóveis<br>
                        <a href="http://goo.gl/YDZdFt" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para Corretores de seguros<br>
                        <a href="http://goo.gl/E7dZgL" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                </tr>
                <tr><td height="5"></td></tr>
                <tr>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para<br> Diretor<br>
                        <a href="http://goo.gl/bm6UYP" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para Empresários<br>
                        <a href="http://goo.gl/KCXIpO" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                </tr>
                <tr><td height="5"></td></tr>
                <tr>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para Engenheiros<br>
                        <a href="http://goo.gl/CH2FWP" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                    <td align="center">
                        Seguro de Responsabilidade Civil Profissional para<br> Eventos<br>
                        <a href="http://goo.gl/KcUv5R" target="_blank"><button class="btn btn-2 btn-2c">Simule e Contrate Online</button></a>
                    </td>
                </tr>
            </table>
                    <br><br>
        </span>
    </div>
</div>
