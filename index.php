<?php

require("login/common.php");

if (empty($_SESSION['user'])) {
    require("modulos/cabecalho-login.php");
} else {
    require("modulos/cabecalho.php");
}

require("modulos/head-body.php");
require("modulos/topo.php");
//require("modulos/menu.php");
require("modulos/slide.php");
require("modulos/cont-inicial.php");
require("modulos/rodape.php");
require("modulos/fim-body.php");

?>



